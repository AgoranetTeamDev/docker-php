.PHONY = all build

all: build

build:
	docker build -t guillaumeagoranet2/php:7.2 -t guillaumeagoranet2/php:latest 7.2
	docker build -t guillaumeagoranet2/php:7.1 7.1
	docker build -t guillaumeagoranet2/php:7.0 7.0
	docker build -t guillaumeagoranet2/php:5.6 5.6

publish:
	docker push guillaumeagoranet2/php
